package no.ntnu;

import no.ntnu.deck.PlayingCard;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class PlayingCardTest {
  private PlayingCard card;

  @BeforeEach
  void setUp() {
    card = new PlayingCard('S', 1);
  }

  @Test
  void testIllegalFace(){
    assertThrows(IllegalArgumentException.class, () -> {
      PlayingCard card2 = new PlayingCard('S', 0);
    }, "face cant be null");
  }

  @Test
  void testGetAsString() {
    assertEquals("S1", card.getAsString());
  }

  @Test
  void testGetSuit() {
    assertEquals('S', card.getSuit());
  }

  @Test
  void testGetFace() {
    assertEquals(1, card.getFace());
  }

  @Test
  void testToString() {
    assertEquals("PlayingCard{suit=S, face=1}", card.toString());
  }

  @Test
  void testGetFaceImage() {
    assertNotNull(card.getFaceImage());
  }
}
