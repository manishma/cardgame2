package no.ntnu;

import no.ntnu.deck.DeckOfCards;

import no.ntnu.deck.PlayingCard;
import org.junit.jupiter.api.*;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
public class DeckOfCardsTest {
  private DeckOfCards deck;

  @BeforeEach
  void setUp() {
    deck = new DeckOfCards();
  }

  @Test
  void testDeckSize() {
    assertEquals(52, deck.getDeck().size());
  }

  @Test
  void testDealHand() {
    ArrayList<PlayingCard> hand = deck.dealHand(5);
    assertEquals(5, hand.size());
  }

  @Test
  void testSumOfHand() {
    ArrayList<PlayingCard> hand = new ArrayList<>();
    hand.add(new PlayingCard('H', 10));
    hand.add(new PlayingCard('S', 3));
    hand.add(new PlayingCard('D', 6));

    int sum = deck.sumOfHand(hand);
    assertEquals(19, sum);
  }

  @Test
  void testHeartAmount() {
    ArrayList<PlayingCard> hand = new ArrayList<>();
    hand.add(new PlayingCard('H', 10));
    hand.add(new PlayingCard('S', 3));
    hand.add(new PlayingCard('H', 6));

    String heartAmount = deck.heartAmount(hand);
    assertEquals("H10 H6", heartAmount);
  }

  @Test
  void testSpadeQueen() {
    ArrayList<PlayingCard> hand = new ArrayList<>();
    hand.add(new PlayingCard('H', 10));
    hand.add(new PlayingCard('S', 3));
    hand.add(new PlayingCard('S', 12));

    assertTrue(deck.spadeQueen(hand));
  }

  @Test
  void testFlush() {
    ArrayList<PlayingCard> hand = new ArrayList<>();
    hand.add(new PlayingCard('H', 10));
    hand.add(new PlayingCard('H', 3));
    hand.add(new PlayingCard('H', 6));

    assertTrue(deck.flush(hand));
  }

  @Test
  void testNonFlush() {
    ArrayList<PlayingCard> hand = new ArrayList<>();
    hand.add(new PlayingCard('H', 10));
    hand.add(new PlayingCard('S', 3));
    hand.add(new PlayingCard('H', 6));

    assertFalse(deck.flush(hand));
  }
}
