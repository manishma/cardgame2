package no.ntnu.controller;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

import no.ntnu.deck.PlayingCard;
import no.ntnu.deck.DeckOfCards;

import java.util.ArrayList;

/**
 * HelloController class.
 */
public class HelloController {
  /**
   * The Deck and parameters to be used.
   */
  DeckOfCards deck = new DeckOfCards();
  private ArrayList<PlayingCard> cards = new ArrayList<>();

  private Button dealHandButton;
  private Button checkHandButton;
  private TextField sumOfFaces;
  private TextField cardsOfHearts;
  private TextField flush;
  private TextField queenOfSpades;

  private ImageView img1;
  private ImageView img2;
  private ImageView img3;
  private ImageView img4;
  private ImageView img5;

  /**
   * Instantiates a new Hello controller.
   *
   * @param dealHandButton  the deal hand button
   * @param checkHandButton the check hand button
   * @param sumOfFaces      the sum of faces
   * @param cardsOfHearts   the cards of hearts
   * @param flush           the flush
   * @param queenOfSpades   the queen of spades
   * @param img1            the img 1
   * @param img2            the img 2
   * @param img3            the img 3
   * @param img4            the img 4
   * @param img5            the img 5
   */
  public HelloController(Button dealHandButton, Button checkHandButton, TextField sumOfFaces, TextField cardsOfHearts, TextField flush, TextField queenOfSpades, ImageView img1, ImageView img2, ImageView img3, ImageView img4, ImageView img5) {
    this.dealHandButton = dealHandButton;
    this.checkHandButton = checkHandButton;
    this.sumOfFaces = sumOfFaces;
    this.cardsOfHearts = cardsOfHearts;
    this.flush = flush;
    this.queenOfSpades = queenOfSpades;
    this.img1 = img1;
    this.img2 = img2;
    this.img3 = img3;
    this.img4 = img4;
    this.img5 = img5;

    // Set event handlers
    this.dealHandButton.setOnAction(e -> this.dealHandAction());
    this.checkHandButton.setOnAction(e -> this.checkHandAction());
  }

  /**
   * Fills Image Views with images based on the card values
   */
  private void fillImages() {
    img1.setImage(cards.get(0).getFaceImage());
    img2.setImage(cards.get(1).getFaceImage());
    img3.setImage(cards.get(2).getFaceImage());
    img4.setImage(cards.get(3).getFaceImage());
    img5.setImage(cards.get(4).getFaceImage());
  }

  /**
   * Gets 5 Random cards in deck and empties the field, fills images with fillImages()
   */
  private void dealHandAction() {
    cards.clear();
    cards = deck.dealHand(5);
    sumOfFaces.setText("");
    cardsOfHearts.setText("");
    flush.setText("");
    flush.setStyle("-fx-border-color: inherit;");
    queenOfSpades.setText("");
    queenOfSpades.setStyle("-fx-border-color: inherit;");
    fillImages();
  }

  /**
   * Checks if the 5 cards meet the requirements for each of the 4 fields,
   * and updates them with some styling
   */
  private void checkHandAction() {
    sumOfFaces.setText(String.valueOf(deck.sumOfHand(cards)));
    cardsOfHearts.setText(deck.heartAmount(cards));
    boolean flushState = deck.flush(cards);
    flush.setText(flushState ? "Yes" : "No");
    flush.setStyle(flushState ? "-fx-background-color: #21e065;" : "-fx-background-color: #e02121;");
    boolean queenOfSpadesStatus = deck.spadeQueen(cards);
    queenOfSpades.setText(queenOfSpadesStatus ? "Yes" : "No");
    queenOfSpades.setStyle(queenOfSpadesStatus ? "-fx-background-color: #21e065;" : "-fx-background-color: #e02121;");
  }
}


