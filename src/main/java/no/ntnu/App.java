package no.ntnu;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import no.ntnu.controller.HelloController;

/**
 * GUI App
 */
public class App extends Application {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        launch();
    }

    /**
     * Overrides start function
     */
    @Override
    public void start(Stage stage) {
        /**
         * Title on App
         */
        VBox root = new VBox();
        root.setAlignment(javafx.geometry.Pos.CENTER);
        root.setPrefHeight(516.0);
        root.setPrefWidth(631.0);
        root.setSpacing(20.0);
        root.setPadding(new Insets(20.0));

        Label title = new Label("Cardgame");
        title.setFont(new Font(40.0));
        VBox.setMargin(title, new Insets(20.0, 0, 0, 0));

        HBox hBox = new HBox();
        hBox.setPrefHeight(326.0);
        hBox.setPrefWidth(591.0);

        /**
         * Images views
         */
        for (int i = 0; i < 5; i++) {
            Region region = new Region();
            region.setPrefHeight(200.0);
            region.setPrefWidth(500.0);

            ImageView imageView = new ImageView();
            imageView.setFitHeight(220.0);
            imageView.setFitWidth(150.0);
            imageView.setPickOnBounds(true);
            imageView.setPreserveRatio(true);
            HBox.setMargin(imageView, new Insets(5.0, i == 4 ? 10.0 : 0, 0, 10.0));
            if (i != 4){
                hBox.getChildren().addAll(region, imageView);
            }else {
                Region region2 = new Region();
                region2.setPrefHeight(200.0);
                region2.setPrefWidth(500.0);
                hBox.getChildren().addAll(region, imageView, region2);
            }
        }

        /**
         * Bottom of app with inputs fields and buttons
         */
        HBox hBox2 = new HBox();
        hBox2.setPrefHeight(100.0);
        hBox2.setPrefWidth(200.0);

        Region region1 = new Region();
        region1.setPrefHeight(157.0);
        region1.setPrefWidth(52.0);
        hBox2.getChildren().add(region1);

        AnchorPane anchorPane = new AnchorPane();
        anchorPane.setPrefHeight(159.0);
        anchorPane.setPrefWidth(788.0);

        Button dealHandButton = new Button("Deal Hand");
        dealHandButton.setId("dealHandButton");
        dealHandButton.setFont(new Font(14.0));
        dealHandButton.setPrefHeight(52.0);
        dealHandButton.setPrefWidth(135.0);
        dealHandButton.setLayoutX(591.0);
        dealHandButton.setLayoutY(22.0);

        Button checkHandButton = new Button("Check Hand");
        checkHandButton.setId("checkHandButton");
        checkHandButton.setFont(new Font(14.0));
        checkHandButton.setPrefHeight(52.0);
        checkHandButton.setPrefWidth(135.0);
        checkHandButton.setLayoutX(591.0);
        checkHandButton.setLayoutY(94.0);

        Text sumOfFacesText = new Text("Sum of faces:");
        sumOfFacesText.setFont(new Font(15.0));
        sumOfFacesText.setLayoutX(35.0);
        sumOfFacesText.setLayoutY(58.0);

        TextField sumOfFaces = new TextField();
        sumOfFaces.setLayoutX(134.0);
        sumOfFaces.setLayoutY(39.0);
        sumOfFaces.setPrefHeight(25.0);
        sumOfFaces.setPrefWidth(70.0);
        //sumOfFaces.setDisable(true);

        Text cardsOfHeartsText = new Text("Cards of hearts:");
        cardsOfHeartsText.setFont(new Font(15.0));
        cardsOfHeartsText.setLayoutX(272.0);
        cardsOfHeartsText.setLayoutY(58.0);

        TextField cardsOfHearts = new TextField();
        cardsOfHearts.setLayoutX(386.0);
        cardsOfHearts.setLayoutY(40.0);
        cardsOfHearts.setPrefHeight(30.0);
        cardsOfHearts.setPrefWidth(158.0);
        //cardsOfHearts.setDisable(true);

        Text flushText = new Text("Flush:");
        flushText.setFont(new Font(15.0));
        flushText.setLayoutX(35.0);
        flushText.setLayoutY(121.0);

        TextField flush = new TextField();
        flush.setLayoutX(81.0);
        flush.setLayoutY(102.0);
        flush.setPrefHeight(25.0);
        flush.setPrefWidth(70.0);
        //flush.setDisable(true);

        Text queenOfSpadesText = new Text("Queen of Spades:");
        queenOfSpadesText.setFont(new Font(15.0));
        queenOfSpadesText.setLayoutX(272.0);
        queenOfSpadesText.setLayoutY(121.0);

        TextField queenOfSpades = new TextField();
        queenOfSpades.setLayoutX(399.0);
        queenOfSpades.setLayoutY(103.0);
        queenOfSpades.setPrefHeight(30.0);
        queenOfSpades.setPrefWidth(70.0);
        //queenOfSpades.setDisable(true);

        anchorPane.getChildren().addAll(dealHandButton, checkHandButton, sumOfFacesText, sumOfFaces, cardsOfHeartsText, cardsOfHearts, flushText, flush, queenOfSpadesText, queenOfSpades);

        Region region = new Region();
        region.setPrefHeight(157.0);
        region.setPrefWidth(500.0);
        Region region2 = new Region();
        region2.setPrefHeight(157.0);
        region2.setPrefWidth(500.0);
        hBox2.getChildren().addAll(region, anchorPane, region2);

        root.getChildren().addAll(title, hBox, hBox2);

        /**
         * Controller for App.java
         * Controller is never used again in App.java since it has listeners defined inside its constructor
         */
        HelloController controller = new HelloController(dealHandButton, checkHandButton, sumOfFaces, cardsOfHearts, flush, queenOfSpades, (ImageView) hBox.getChildren().get(1), (ImageView) hBox.getChildren().get(3), (ImageView) hBox.getChildren().get(5), (ImageView) hBox.getChildren().get(7), (ImageView) hBox.getChildren().get(9));

        /**
         * Creates the scene
         */
        Scene scene = new Scene(root);

        /**
         * Adds CSS
         */
        String cssPath = getClass().getResource("/no/ntnu/layout.css").toExternalForm();
        scene.getStylesheets().add(cssPath);

        stage.setScene(scene);
        stage.setTitle("CardGame");
        stage.show();
    }
}