package no.ntnu.deck;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Class DeckOfCards.
 */
public class DeckOfCards {
  /**
   * Parameters for class
   */
  private final char[] suits = {'S','H','D','C'};
  private ArrayList<PlayingCard> deck = new ArrayList<>();

  /**
   * Instantiates a new Deck of cards.
   */
  public DeckOfCards(){
    for (int i = 0; i < suits.length; i++) {
      for (int j = 1; j <= 13; j++) {
        deck.add(new PlayingCard(suits[i],j));
      }
    }

    if(deck.size() != 52) {throw new IllegalArgumentException("Ya deck ain't big enough");}
  }

  /**
   * Gets deck.
   *
   * @return the deck
   */
  public ArrayList<PlayingCard> getDeck() {
    return this.deck;
  }

  /**
   * Deal hand array list.
   *
   * @param i the amount of random cards wanted
   * @return the array list of the random cards
   */
  public ArrayList<PlayingCard> dealHand(int i){
    Random random = new Random();
    ArrayList<PlayingCard> randomCards = new ArrayList<>();
    for (int j = 0; j < i; j++) {
      randomCards.add(deck.get(random.nextInt(52)));
    }
    return randomCards;
  }

  /**
   * Sum of hand int.
   *
   * @param cards the cards that we want to sum
   * @return the int, the sum of the cards
   */
  public int sumOfHand(ArrayList<PlayingCard> cards){
    return cards.stream()
        .mapToInt(PlayingCard::getFace)
        .sum();
  }

  /**
   * Heart amount string.
   *
   * @param cards the cards we want to check heart amount off
   * @return the string containing all heart cards
   */
  public String heartAmount(ArrayList<PlayingCard> cards){
    List<String> hearts = cards.stream()
        .filter(card -> card.getSuit() == 'H')
        .map(card -> "H" + card.getFace())
        .collect(Collectors.toList());

    return hearts.isEmpty() ? "No Hearts" : String.join(" ", hearts);
  }

  /**
   * Spade queen boolean.
   *
   * @param cards the cards we want to check if contain a Queen of Spade
   * @return the boolean value of if Queen of spade exists
   */
  public boolean spadeQueen(ArrayList<PlayingCard> cards){
    for (PlayingCard card : cards) {
      if (card.getFace() == 12 && card.getSuit() == 'S') {
        return true;
      }
    }
    return false;
  }

  /**
   * Flush boolean.
   *
   * @param cards the cards we want to check if are a flush
   * @return the boolean of if they are a flush
   */
  public boolean flush(ArrayList<PlayingCard> cards){
    if (cards.isEmpty()) {return false;}
    char firstSuit = cards.get(0).getSuit();
    return cards.stream().allMatch(card -> card.getSuit() == firstSuit);
  }
}
